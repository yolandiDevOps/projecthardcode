FROM nginx:1.27.0

COPY index.html /usr/share/nginx/html/index.html

EXPOSE 80

RUN apt-get update && apt-get install -y curl

CMD ["nginx", "-g", "daemon off;"]

HEALTHCHECK --interval=30s --timeout=10s --start-period=5s --retries=3 \

CMD curl -f http://localhost/ || exit 1
