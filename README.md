
## Локальный запуск

Команды для запуска:

```
docker build -t nginx-project:1.0.0 .
docker run -d -p 8080:80 nginx-project:1.0.0
git branch -M main
curl http://localhost:8080
```